package com.lybrate.doctor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.Assert;

import com.lybrate.doctor.exception.LybrateException;
import com.lybrate.doctor.service.DDLService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DoctorApplication.class)
@ActiveProfiles("test")
@WebAppConfiguration
public class BaseTest {

  @Autowired
  private DDLService ddlService;

  @Test
  public void contextLoads() {
    Assert.isTrue(Boolean.TRUE);
  }

  @Before
  public void setup() throws LybrateException {}

  @After
  public void tearDown() {
    ddlService.dropDatabase();
  }
}
