package com.lybrate.doctor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.Assert;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import com.lybrate.doctor.service.DDLService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DoctorApplication.class)
@WebAppConfiguration
@ActiveProfiles("test")
public class BaseControllerTest {
  
  @Autowired
  private DDLService ddlService;
  
  @Value("${local.server.port}")
  private int port;


  @Before
  public void setup() {
    RestAssured.port = port;
    RestAssured.defaultParser = Parser.JSON;
  }

  @Test
  public void contextLoads() {
    Assert.isTrue(Boolean.TRUE);
  }
  
  @After
  public void tearDown() {
    ddlService.dropDatabase();
  }
}
