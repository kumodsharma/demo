package com.lybrate.doctor.controller;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.lybrate.doctor.BaseControllerTest;
import com.lybrate.doctor.dto.DoctorDto;
import com.lybrate.doctor.exception.LybrateException;
import com.lybrate.doctor.service.IDoctorService;
import com.lybrate.doctor.util.AppConstants;

public class DoctorControllerTest extends BaseControllerTest {

  private static final String API_PATH = AppConstants.API_PREFIX + "/doctor/";
  
  @Autowired
  private IDoctorService doctorService;
  
  @Before
  public void before() throws LybrateException {
  }
  
  @Test
  public void shouldBeAbleToSaveDoctorProfile() {
    DoctorDto doctor = new DoctorDto("kumod@lybrate.com", "kumod", "kumar", "sharma");
    RestAssured.given()
        .body(doctor).when().contentType(ContentType.JSON).post(API_PATH+"profile").then()
        .statusCode(HttpStatus.OK.value()).body("data.firstName", Matchers.is("kumod"));
  }
}
