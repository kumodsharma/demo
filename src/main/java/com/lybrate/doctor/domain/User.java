package com.lybrate.doctor.domain;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.index.Indexed;

public class User extends AbstractMongoEntity {

  private static final long serialVersionUID = -4790744939447304091L;

  @Indexed(unique = true)
  @NotBlank(message = "Email can't be blank")
  private String email;

  private String firstName;

  private String middleName;

  private String lastName;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public User() {
    super();
  }
  
  public User(String email, String firstName, String middleName, String lastName) {
    super();
    this.email = email;
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
  }

  @Override
  public String toString() {
    return "User [email=" + email + ", firstName=" + firstName + ", middleName=" + middleName
        + ", lastName=" + lastName + "]";
  }


}
