package com.lybrate.doctor.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lybrate.doctor.dto.DoctorDto;
import com.lybrate.doctor.exception.LybrateException;
import com.lybrate.doctor.service.IDoctorService;
import com.lybrate.doctor.transformer.DoctorTansformer;
import com.lybrate.doctor.util.AppConstants;
import com.lybrate.doctor.util.RestResponse;
import com.lybrate.doctor.util.RestUtils;

@RestController
@RequestMapping(AppConstants.API_PREFIX + "/doctor")
public class DoctorController {

  private static final Logger LOGGER = LoggerFactory.getLogger(DoctorController.class);

  @Autowired
  private IDoctorService doctorService;

  @Autowired
  private DoctorTansformer doctorTansformer;

  @RequestMapping(value = "/profile", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<RestResponse<DoctorDto>> saveDoctor(DoctorDto doctorDto)
          throws LybrateException {
    LOGGER.info("creating  doctor : " + doctorDto);
    return RestUtils.successResponse(doctorTansformer
        .toDto(doctorService.saveProfile(doctorDto)));
  }
}
