package com.lybrate.doctor.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.lybrate.doctor.domain.User;
import com.lybrate.doctor.dto.DoctorDto;

@Service
public class DoctorTansformer extends GenericTransformer<User, DoctorDto> {

  @Override
  public DoctorDto toDto(User user) {
    DoctorDto userDto = new DoctorDto();
    BeanUtils.copyProperties(user, userDto);
    return userDto;
  }
}
