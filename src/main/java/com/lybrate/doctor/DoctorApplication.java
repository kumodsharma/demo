package com.lybrate.doctor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@EnableAutoConfiguration
@SpringBootApplication
@EnableMongoAuditing
public class DoctorApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
     System.out.println("hello");
        SpringApplication.run(DoctorApplication.class, args);
    }
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
      return application.sources(applicationClass);
    }

    private static Class<DoctorApplication> applicationClass = DoctorApplication.class;
}
