package com.lybrate.doctor.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotBlank;

public class DoctorDto implements Serializable {

  private static final long serialVersionUID = -521264685655989220L;

  @NotBlank
  private String email;

  @NotBlank
  private String firstName;

  @NotBlank
  private String middleName;

  @NotBlank
  private String lastName;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public DoctorDto(String email, String firstName, String middleName, String lastName) {
    this.email = email;
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
  }

  public DoctorDto() {
    super();
  }

  @Override
  public String toString() {
    return "DoctorDto [email=" + email + ", firstName=" + firstName + ", middleName=" + middleName
        + ", lastName=" + lastName + "]";
  }
  
  
}

