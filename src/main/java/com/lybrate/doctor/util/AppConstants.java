package com.lybrate.doctor.util;

public interface AppConstants {

  String API_PREFIX = "/api/v1";

  String AUTH_TOKEN = "X-AUTH-TOKEN";

}
