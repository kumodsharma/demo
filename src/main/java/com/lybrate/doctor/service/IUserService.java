package com.lybrate.doctor.service;

import org.springframework.stereotype.Service;

import com.lybrate.doctor.domain.User;
import com.lybrate.doctor.dto.DoctorDto;
import com.lybrate.doctor.exception.LybrateException;

@Service("userService")
public interface IUserService<T extends User> extends GenericService<T> {

 // T findByEmail(String email);

  T saveProfile(DoctorDto userDto) throws LybrateException;

}
