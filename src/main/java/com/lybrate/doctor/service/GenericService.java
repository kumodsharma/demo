package com.lybrate.doctor.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lybrate.doctor.domain.AbstractMongoEntity;
import com.lybrate.doctor.exception.LybrateException;

/**
 * This interface abstracts out the CRUD services for any domain.
 * 
 * @param <T> specifies the Domain object it is working on. It assumes a string type of ID.
 */
public interface GenericService<T extends AbstractMongoEntity> {

  T findOne(String id) throws LybrateException;

  T save(T domain) throws LybrateException;

  List<T> save(List<T> domains) throws LybrateException;

  Page<T> findAll(Pageable page);

  List<T> findAll();

  void delete(String id) throws LybrateException;

  T update(String id, T domain) throws LybrateException;

}
