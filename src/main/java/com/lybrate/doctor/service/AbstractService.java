package com.lybrate.doctor.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.lybrate.doctor.domain.AbstractMongoEntity;
import com.lybrate.doctor.exception.LybrateException;
import com.lybrate.doctor.service.GenericService;


public abstract class AbstractService<T extends AbstractMongoEntity> implements GenericService<T> {

  @Autowired
  private MongoRepository<T, String> mongoRepository;

  @Override
  public T findOne(String id) throws LybrateException {
    T entity = mongoRepository.findOne(id);
    if (entity == null) {
      throw notFoundException();
    }
    return entity;
  }

  @Override
  public List<T> findAll() {
    return mongoRepository.findAll();
  }

  @Override
  public Page<T> findAll(Pageable page) {
    return mongoRepository.findAll(page);
  }

  @Override
  public void delete(String id) throws LybrateException {
    T entity = findOne(id);
    mongoRepository.delete(entity);
  }

  @Override
  public T save(T domain) throws LybrateException {
    return mongoRepository.save(domain);
  }

  @Override
  public List<T> save(List<T> domains) throws LybrateException {
    return mongoRepository.save(domains);
  }

  @Override
  public T update(String id, T domain) throws LybrateException {
    T fromDb = findOne(id);
    domain.copyEntityFrom(fromDb);
    return save(domain);
  }

  protected abstract LybrateException notFoundException();
}
