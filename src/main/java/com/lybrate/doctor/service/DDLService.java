package com.lybrate.doctor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class DDLService {

  @Autowired
  private MongoTemplate mongoTemplate;

  public void dropDatabase() {
    mongoTemplate.getDb().dropDatabase();
  }
}
