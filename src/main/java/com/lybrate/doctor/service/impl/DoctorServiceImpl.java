package com.lybrate.doctor.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.lybrate.doctor.domain.Doctor;
import com.lybrate.doctor.dto.DoctorDto;
import com.lybrate.doctor.exception.EntityNotFoundException;
import com.lybrate.doctor.exception.LybrateException;
import com.lybrate.doctor.service.AbstractUserService;
import com.lybrate.doctor.service.IDoctorService;

@Service
public class DoctorServiceImpl extends AbstractUserService<Doctor> implements IDoctorService {

  @Override
  protected LybrateException notFoundException() {
    return new EntityNotFoundException("Doctor details not found !");
  }

  @Override
  public Doctor saveProfile(DoctorDto doctorDto) throws LybrateException {
    Doctor doctor = new Doctor();
    BeanUtils.copyProperties(doctor, doctorDto);
    return save(doctor);
  }

}
