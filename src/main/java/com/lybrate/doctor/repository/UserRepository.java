package com.lybrate.doctor.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lybrate.doctor.domain.User;

public interface UserRepository<T extends User> extends MongoRepository<T, String> {

  T findByEmail(String email);
}
