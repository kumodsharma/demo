package com.lybrate.doctor.repository;

import com.lybrate.doctor.domain.Doctor;

public interface DoctorRepository extends UserRepository<Doctor> {

}
